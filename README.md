# Oddny Ajax Cart

Update the cart page using AJAX

## Installation
`modman clone git@bitbucket.org:oddnyse/oddny_ajaxcart.git`

Since we use modman, don't forget to set System / Configuration / Developer / Template Settings / Allow Symlinks to 'Yes'


## Usage
1. Required – Add the following to the cart table form:
    * Class: `oddny-ajax-cart js-oddny-cart-form`
    * Action: `<?php echo $this->getUrl('ajaxcart/cart/updateshoppingcart') ?>`
2. Required – Add the following to each quantity input:
    * Class: `js-oddny-cart-input`
    * Data attribute: `data-oddny-cart-item="XXX"`, XXX should be replaced with a unique identifier, preferably the product ID
3. Optional – Add the following to the remove/delete item element:
    * Class: `js-oddny-cart-remove`
    * Data attribute: `data-oddny-cart-item="XXX"`, XXX should be replaced with the same identifier added to the quantity input
    * Data attrobute: `data-oddny-cart-title="XXX"`, XXX should preferably be the item title/name
3. Optional – If you're not using the HTML5 `input type="number"` and wish to add plus/minus buttons to the quantity input, create one element for decrement and one for increment. Those elements should have the following class and data attributes:
    * Class: `js-oddny-cart-qty`
    * Data attribute: `data-oddny-cart-item="XXX"`,  XXX should be replaced with the same identifier added to the quantity input
    * Data attribute: `data-oddny-cart-action="XXX"`, XXX should be either `decrement` or `increment`
4. Optional - If you want the coupon form to update the page using AJAX, then add the following to the coupon form:
    * Class: `js-oddny-coupon-form`
    * Action: `<?php echo $this->getUrl('ajaxcart/cart/oddnycouponPost') ?>`


## Remove an item
By default Oddny Ajax Cart will display an inline confirmation when a user clicks the delete item element. If you wish to change this to a regular modal dialog add the following snippet in `oddny/ajaxcart/cart-container.phtml`:

```
#!js

<script type="text/javascript">
oddnyCart.setConfirmType('alert');
</script>
```

## Events

There's a couple of events that's fired and ready for you to hook on to.

Event                    | When?
------------------------ | ------------------------
`oddnyCartInit`          | Fires on page load
`oddnyCartAjaxBefore`    | Fires before the cart is updated
`oddnyCartAjaxAfter`     | Fires after the cart is updated
`oddnyCartQtyChange`     | Fires when the quantity input has changed, either by user input or by the quantity buttons
`oddnyCartQtyButton`     | Fires when a user clicks any of the `.js-oddny-cart-qty` elements
`oddnyCartRemove`        | Fires when the user clicks the `.js-oddny-cart-remove` element
`oddnyCartRemoveCancel`  | Fires if the user cancels the remove item action
`oddnyCartRemoveConfirm` | Fires if the user confirms the removal of the item
`oddnyCartCouponBefore`  | Fires before the coupon form is submitted
`oddnyCouponAjaxAfter`   | Fires after the coupon form is submitted

Example:
```
#!js

$(document).on('oddnyCartRemoveCancel', function() {
    alert('Great, thanks for making us more money!');
});
```
