var oddnyCart = (function ($) {

    'use strict';

    //--------------------
    //  Global variables
    //--------------------

    var $_DOCUMENT = $(document);


    //-----------------------
    //  Cart table
    //-----------------------

    var cartTable = {

        $confirmType: 'inline',
        $confirmText: '',
        $confirmTextCancel: '',
        $confirmTextConfirm: '',

        /**
         * Bind events
         */
        init: function() {

            $_DOCUMENT.trigger('oddnyCartInit');

            // Form submit
            $_DOCUMENT.on('submit.oddnyCart', '.js-oddny-cart-form', function(e) {
                e.preventDefault();
                $_DOCUMENT.trigger('oddnyCartAjaxBefore');
                cartTable.formSubmit( $(this) );
            });

            // Input change
            $_DOCUMENT.on('change.oddnyCart', '.js-oddny-cart-input', function() {
                if ( !isNaN($(this).val()) ) {
                    $('.js-oddny-cart-form').submit();
                }
                $_DOCUMENT.trigger('oddnyCartQtyChange');
            });

            // Plus / Minus click
            $_DOCUMENT.on('click.oddnyCart', '.js-oddny-cart-qty', function(e) {
                var $self = $(this);

                cartTable.changeQty( $self.data('oddny-cart-action'), $self.data('oddny-cart-item') );
                $_DOCUMENT.trigger('oddnyCartQtyButton');
            });

            // Remove item link click
            $_DOCUMENT.on('click.oddnyCart', '.js-oddny-cart-remove', function(e) {
                e.preventDefault();

                var $self = $(this);

                cartTable.remove( $self.data('oddny-cart-item'), $self.data('oddny-cart-title') );
            });

            // Remove the confirmation "dialog"
            $_DOCUMENT.on('click.oddnyCart', '.js-oddny-cart-delete-cancel', function(e) {
                cartTable.removeCancel();
                $_DOCUMENT.trigger('oddnyCartRemoveCancel');
            });

            // Sure, let us delete the item from the cart
            $_DOCUMENT.on('click.oddnyCart', '.js-oddny-cart-delete-confirm', function(e) {
                cartTable.removeAction( $(this).data('item') );
                $_DOCUMENT.trigger('oddnyCartRemoveConfirm');
            });

        }, // cartTable.init


        /**
         * Updates the input
         * @param  {string} $action  Decrement or Increment
         * @param  {string} $itemId  The input ID for the input that should get updated
         */
        changeQty: function( $action, $itemId ) {

            var $input    = $('input[data-oddny-cart-item="' + $itemId + '"]'),
                $quantity = parseInt( $input.val() );

            if ( $action === 'decrement' ) {
                // If it isn't undefined or its greater than 0 we'll decrement
                // otherwise we'll add a 0
                if ( !isNaN( $quantity ) && $quantity > 0 ) {
                    $input.val( $quantity - 1 );
                } else {
                    $input.val( 0 );
                    return;
                }
            }
            else if ( $action === 'increment' ) {
                // If it isn't undefined lets increment
                // otherwise we'll add a 0
                if ( !isNaN( $quantity ) ) {
                    $input.val( $quantity + 1 );
                }
                else {
                    $input.val( 0 );
                    return;
                }
            }

            $input.change();

        }, // cartTable.changeQty


        /**
         * Sets the item input to 0 and then triggers the input change
         * @param  {string} $itemId    The input ID for the input that should get updated
         * @param  {string} $itemTitle Item title
         */
        remove: function( $itemId, $itemTitle ) {

            cartTable.removeCancel();

            if ( cartTable.$confirmType === 'inline' && window.matchMedia('(min-width: 768px)').matches ) {
                $('.js-oddny-cart-form').addClass('has-overlay').append(
                    '<div class="oddny-ajax-cart__alert js-oddny-cart-alert">' +
                    '<p>' + cartTable.sprintf( cartTable.$confirmText, $itemTitle ) + '</p>' +
                    '<button class="oddny-ajax-cart__button js-oddny-cart-delete-cancel">' + cartTable.$confirmTextCancel + '</button>' +
                    '<button class="oddny-ajax-cart__button js-oddny-cart-delete-confirm" data-item="' + $itemId + '">' + cartTable.$confirmTextConfirm + '</button>' +
                    '</div>'
                );
            }
            else {
                var $text = cartTable.sprintf(cartTable.$confirmText, $itemTitle).replace(/<(?:.|\n)*?>/gm, '');
                if ( window.confirm( $text ) ) {
                    cartTable.removeAction( $itemId );
                }
            }

        }, // cartTable.remove


        /**
         * Sets the item input to 0 and then triggers the input change
         * @param  {string}  $itemId  The input ID for the input that should get updated
         */
        removeAction: function( $itemId ) {

            cartTable.removeCancel();
            $('input[data-oddny-cart-item="' + $itemId + '"]').val( 0 ).change();

        }, // cartTable.removeAction


        /**
         * Removes the confirmation alert
         */
        removeCancel: function() {

            $('.js-oddny-cart-alert').remove();
            $('.js-oddny-cart-form').removeClass('has-overlay');

        }, // cartTable.removeCancel


        /**
         * * Submit the form and update content
         * @param  {element} $form the submitted form
         */
        formSubmit: function( $form ) {

            $form.addClass('is-loading has-overlay');

            $.ajax({
                type: 'POST',
                url: $form.prop('action'),
                data: $form.serialize(),
                dataType: 'json',

                success: function($response) {
                    $('.js-oddny-ajaxcart-container').html( $response );
                },
                error: function($xhr, $textStatus, $errorThrown) {
                    alert('An error occurred, please copy the error below and report this to the owner of the website:\n' + $textStatus);
                },
                complete: function ($jqXHR, $textStatus) {
                    $_DOCUMENT.trigger('oddnyCartAjaxAfter');
                }
            });

        }, // cartTable.formSubmit


        /**
         * Define the confirm dialog type,
         * @param {string} $type inline | alert
         */
        setConfirmType: function( $type ) {

            cartTable.$confirmType = $type;

        }, // cartTable.setConfirmType


        /**
         * Basic php sprintf port
         * @return {string} Returns a string produced according to the formatting string
         */
         sprintf: function() {

            var $args   = arguments,
                $string = $args[0],
                $i      = 1;

            return $string.replace(/%((%)|s|d)/g, function( $m ) {
                var $val = null;

                if ( $m[2] ) {
                    $val = $m[2];
                }
                else {
                    $val = $args[$i];

                    switch( $m ) {
                        case '%d':
                            $val = parseFloat($val);

                            if ( isNaN($val) ) {
                                $val = 0;
                            }
                            break;
                    }
                    $i++;
                }

                return $val;
            });

         }

    };



    //-----------------------
    //  Coupon on cart page
    //-----------------------

    var cartCoupon = {

        /**
         * Bind events
         */
        init: function() {

            // Form submit
            $_DOCUMENT.on('submit.oddnyCoupon', '.js-oddny-coupon-form', function(e) {
                e.preventDefault();
                $_DOCUMENT.trigger('oddnyCartCouponBefore');
                cartCoupon.formSubmit( $(this) );
            });

        }, // cartCoupon.init


        /**
         * * Submit the form and update content
         * @param  {element} $form the submitted form
         */
        formSubmit: function( $form ) {

            $form.addClass('is-loading has-overlay');

            $.ajax({
                type: 'POST',
                url: $form.prop('action'),
                data: $form.serialize(),
                dataType: 'json',

                success: function($response) {
                    $('.js-oddny-ajaxcart-container').html( $response );
                },
                error: function($xhr, $textStatus, $errorThrown) {
                    alert('An error occurred, please copy the error below and report this to the owner of the website:\n' + $textStatus);
                },
                complete: function ($jqXHR, $textStatus) {
                    $_DOCUMENT.trigger('oddnyCouponAjaxAfter');
                }
            });

        } // cartTable.formSubmit

    };



    //-----------------------
    //  Add to cart AJAX
    //-----------------------

    // To be added...


    //-----------------------
    //  The DOM is ready
    //-----------------------

    $(function() {

        cartTable.$confirmText = Translator.translate("Are you sure you want to remove <strong>%s</strong>?");
        cartTable.$confirmTextCancel = Translator.translate("Cancel");
        cartTable.$confirmTextConfirm = Translator.translate("Remove");
        cartTable.init();
        cartCoupon.init();

    });



    //--------------------
    // Hello public!
    //--------------------

    return {

        setConfirmType: cartTable.setConfirmType

    };

})(jQuery);
