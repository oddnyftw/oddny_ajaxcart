

var gulp       = require('gulp'),
    $plugins   = require('gulp-load-plugins')();

var $project   = 'Oddny Ajax Cart',
    $filenames = {
        css : 'ajaxcart',
        js  : 'ajaxcart'
    },
    $src       = {
        less   : '../_src/less',
        js     : '../_src/js'
    },
    $dist      = {
        css    : '../css',
        js     : '../js'
    };


// Build LESS
gulp.task('dist-css', function() {
    var onSuccess = msgSuccess('CSS/Less', 'Build finished running'),
        onError = function( err ) {
            msgError(err, 'CSS/Less');
            this.emit('end');
        };

    gulp.src( $src.less + '/ajaxcart.less' )
        .pipe( $plugins.plumber({errorHandler: onError}) )
        .pipe( $plugins.changed($dist.css) )
        .pipe( $plugins.sourcemaps.init() )
        .pipe( $plugins.less() )
        // .pipe( $plugins.combineMq({beautify: true}))
        .pipe( $plugins.autoprefixer({browsers: ['last 2 versions']}) )
        .pipe( $plugins.rename($filenames.css + '.css') )
        .pipe( $plugins.sourcemaps.write('sourcemaps') )
        .pipe( gulp.dest($dist.css) )
        .pipe( $plugins.rename({suffix: '.min'}) )
        .pipe( $plugins.minifyCss() )
        .pipe( gulp.dest($dist.css) )
        .pipe( onSuccess )
});


// Concat, lint and minify site specific JS
gulp.task('dist-js-site', function() {
    var onSuccess = msgSuccess('JS (site)', 'Build finished running'),
        onError = function( err ) {
            msgError(err, 'JS (site)');
            this.emit('end');
        };

    gulp.src( [$src.js + '/general.js', $src.js + '/*.js'] )
        .pipe( $plugins.plumber({errorHandler: onError}) )
        .pipe( $plugins.jshint() )
        .pipe( $plugins.jshint.reporter('jshint-stylish') )
        .pipe( $plugins.concat($filenames.js + '.js', {newLine: '\n\n\n'}) )
        .pipe( gulp.dest($dist.js) )
        .pipe( $plugins.bytediff.start() )
        .pipe( $plugins.rename({suffix: '.min'}) )
        .pipe( $plugins.uglify() )
        .pipe( gulp.dest($dist.js) )
        .pipe( $plugins.bytediff.stop(bytediffFormatter) )
        .pipe( onSuccess )
});


// Concat, lint and minify vendor JS
gulp.task('dist-js-vendor', function() {
    var onSuccess = msgSuccess('JS (vendor)', 'Build finished running'),
        onError   = function( err ) {
            msgError(err, 'JS (vendor)');
            this.emit('end');
        };

    gulp.src( $src.js + '/vendor/*.js' )
        .pipe( $plugins.concat($filenames.js + '.vendor.js') )
        .pipe( gulp.dest($dist.js) )
        .pipe( $plugins.bytediff.start() )
        .pipe( $plugins.rename({suffix: '.min'}) )
        .pipe( $plugins.uglify() )
        .pipe( gulp.dest($dist.js) )
        .pipe( $plugins.bytediff.stop(bytediffFormatter) )
        .pipe( onSuccess )
});


// Watch task
gulp.task('watch', function() {
    gulp.watch( $src.less + '/**/*.less', ['dist-css'] );
    gulp.watch( [$src.js + '/**/*.js', '!' + $src.js + '/vendor/*.js'], ['dist-js-site'] );
    gulp.watch( $src.js + '/vendor/*.js', ['dist-js-vendor'] );
});


// Default Gulp task
  gulp.task('default', ['dist-css', 'dist-js-site', 'dist-js-vendor']);


function bytediffFormatter( $data ) {
    var formatPercent = function( $num, $precision ) {
        return ($num * 100).toFixed($precision);
    };
    var difference = ($data.savings > 0) ? ' smaller.' : ' larger.';

    return $data.fileName + ' went from ' +
        ($data.startSize / 1000).toFixed(2) + ' kB to ' + ($data.endSize / 1000).toFixed(2) + ' kB' +
        ' and is ' + formatPercent(1 - $data.percent, 2) + '%' + difference;
}

function msgError( $err, $title ) {
    var message = $plugins.notify.onError({
        title:    "Gulp – " + $project,
        subtitle: $title,
        message:  "Error: <%= error.message %>",
        sound:    "Frog"
    })($err);

    return message;
}

function msgSuccess( $title, $message ) {
    var message = $plugins.notify({
        title:    "Gulp – " + $project,
        subtitle: $title,
        message:  "Success: " + $message,
        sound:    "Tink",
        onLast:   true
    });

    return message;
}
